import { defineCustomElement as VueDefineCustomElement, h, createApp, getCurrentInstance } from 'vue'

export const defineCustomElement = (component, { components = [], plugins = [] }) => {
    component.setup = () => {
        const app = createApp();

        plugins.forEach(app.use);

        components.forEach(e => app.component(e.name, e.component));

        const inst = getCurrentInstance();
        Object.assign(inst.appContext, app._context);
        Object.assign(inst.provides, app._context.provides);
    };
    return VueDefineCustomElement(component);

}
