const rootUrl = 'chunks';

let getChunk = (id) => {
    return window.API.get(`${rootUrl}/${id}`);
  },
  getChunks = () => {
    return window.API.get(`${rootUrl}/`);
  },
  createChunk = (data) => {
    return window.API.post(`${rootUrl}/`, data);
  },
  copyChunk = (id) => {
    return window.API.post(`${rootUrl}/${id}/copy`);
  },
  editChunk = (data) => {
    return window.API.put(`${rootUrl}/${data.id}`, data);
  },
  deleteChunk = (id) => {
    return window.API.delete(`${rootUrl}/${id}`);
  };

export default {
  getChunk,
  getChunks,
  createChunk,
  copyChunk,
  editChunk,
  deleteChunk,
}