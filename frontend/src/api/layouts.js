const rootUrl = 'layouts';

let getLayout = (id) => {
    return window.API.get(`${rootUrl}/${id}`);
  },
  getLayouts = () => {
    return window.API.get(`${rootUrl}/`);
  },
  createLayout = (data) => {
    return window.API.post(`${rootUrl}/`, data);
  },
  copyLayout = (id) => {
    return window.API.post(`${rootUrl}/${id}/copy`);
  },
  editLayout = (data) => {
    return window.API.put(`${rootUrl}/${data.id}`, data);
  },
  deleteLayout = (id) => {
    return window.API.delete(`${rootUrl}/${id}`);
  };

export default {
  getLayout,
  getLayouts,
  createLayout,
  copyLayout,
  editLayout,
  deleteLayout,
}