const rootUrl = 'files';

let getFile = (id) => {
    return window.API.get(`${rootUrl}/${id}`);
  },
  getFiles = () => {
    return window.API.get(`${rootUrl}/`);
  },
  createFile = (data) => {
    return window.API.postMultipart(`${rootUrl}/`, data);
  },
  editFile = (id, data) => {
    return window.API.put(`${rootUrl}/${id}`, data);
  },
  deleteFile = (id) => {
    return window.API.delete(`${rootUrl}/${id}`);
  };

export default {
  getFile,
  getFiles,
  createFile,
  editFile,
  deleteFile,
}