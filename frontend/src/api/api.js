const
  baseUrl = location.hostname,
  protocol = location.protocol,
  port = '8000',
  apiUrl = 'api';

class Api {
  async getUrl(url) {
    return `${protocol}//${baseUrl}:${port}/${apiUrl}/${url}`;
  }
  getPath() {
    return window.location.pathname.split('/');
  }

  getToken() {
    return localStorage.getItem('access_token');
  }
  getExpIn() {
    return localStorage.getItem('expires_in');
  }
  setToken(data) {
    const date = Date.now() + data.expires_in * 1000;
    localStorage.setItem('access_token', data.access_token);
    localStorage.setItem('expires_in', date);
  }
  removeToken() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('expires_in');
  }

  validToken() {
    const tolerance = 5 * 1000;
    let exp_in = this.getExpIn();
    if(exp_in - Date.now() > tolerance) {
      return this.getToken();
    }
    return null;
  }

  get(url, data) {
    return this.send(url, data, 'GET');
  }
  post(url, data) {
    return this.send(url, data, 'POST');
  }
  async postMultipart(url, formData) {
    url = await this.getUrl(url);
    return fetch(
        url,
        {
          method: 'POST',
          body: formData,
          headers: {
            'Access_token': this.validToken(),
          }
        },
    ).then(r => {
      return this.checkErrors(r);
    })
  }
  put(url, data) {
    return this.send(url, data, 'PUT');
  }
  patch(url, data) {
    return this.send(url, data, 'PATCH');
  }
  delete(url, data) {
    return this.send(url, data, 'DELETE');
  }

  needAuth(url) {
    return ![
      'login',
      'logout',
      'refresh',
    ].includes(url);
  }

  async send(url, data, method) {
    let token = this.validToken();
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      init = {
        method: method,
        headers: headers,
      };
    if(this.needAuth(url)) {
      if(token === null) {
        console.log('Token Expired');
        return;
      } else {
        headers['Access_token'] = token;
      }
    }
    url = await this.getUrl(url);
    if (method !== 'GET') {
      init.body = JSON.stringify(data);
    } else {
      if(data) {
        url += '?' + Object.keys(data)
          .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(data[k]))
          .join('&');
      }
    }
    return fetch(
      url,
      init,
    ).then(r => {
      return this.checkErrors(r);
    })
  }

  async checkErrors(result) {
    let data;
    if(result.ok) {
      data = await result.json();
      let response = {
        response: {
          status: result.status,
          statusText: result.statusText,
          data: data,
        }
      };
      return response.response;
    } else {
      console.log('Something happened!');
      if(result.data !== undefined) {
        console.log(result.data.error);
      }
      return {data: {}};
    }
    // if(result.ok) {
    //   return response.response;
    // } else {
    //   if(data.hasOwnProperty('errors')) {
    //     for(let key in data.errors){
    //       Vue.notify({
    //         text: i18n.t('registration.' + key) + " " + i18n.t(`error.${data.errors[key]}`),
    //         type: 'error'
    //       });
    //     }
    //   }
    //   if(data.hasOwnProperty('error')) {
    //     Vue.notify({
    //       text: i18n.t(`error.${data.error}`),
    //       type: 'error',
    //     });
    //   }
    //   throw response;
    // }
  }
}

window.API = new Api();
