const rootUrl = 'common';

let getPage = (data) => {
        return window.API.get(`${rootUrl}/page`, data);
    },
    getLayout = (id) => {
        return window.API.get(`${rootUrl}/layout/${id}`);
    },
    getMenu = (name) => {
        return window.API.get(`${rootUrl}/menu/${name}`);
    },
    getChunk = (name) => {
        return window.API.get(`${rootUrl}/chunk/${name}`);
    },
    getImage = (data) => {
        return window.API.get(`${rootUrl}/image`, data);
    };

export default {
    getPage,
    getLayout,
    getMenu,
    getChunk,
    getImage,
}