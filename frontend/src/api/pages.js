const rootUrl = 'pages';

let getPage = (id) => {
    return window.API.get(`${rootUrl}/${id}`);
  },
  getPages = () => {
    return window.API.get(`${rootUrl}/`);
  },
  createPage = (data) => {
    return window.API.post(`${rootUrl}/`, data);
  },
  copyPage = (id) => {
    return window.API.post(`${rootUrl}/${id}/copy`);
  },
  editPage = (data) => {
    return window.API.put(`${rootUrl}/${data.id}`, data);
  },
  deletePage = (id) => {
    return window.API.delete(`${rootUrl}/${id}`);
  };

export default {
  getPage,
  getPages,
  createPage,
  copyPage,
  editPage,
  deletePage,
}