let getCurrentUser = () => {
    return window.API.get('user-profile');
  },
  login = (data) => {
    return window.API.post('login', data)
      .then(r => {
        window.API.setToken(r.data);
        return r;
      });
  },
  logout = () => {
    return window.API.post('logout')
      .then(r => {
        window.API.removeToken(r.data);
        return r;
      });
  },
  refresh = () => {
    return window.API.post('refresh')
      .then(r => {
        window.API.setToken(r.data);
        return r;
      });
  };

export default {
  getCurrentUser,
  login,
  logout,
  refresh,
}