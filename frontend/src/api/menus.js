const rootUrl = 'menus';

let getMenu = (id) => {
    return window.API.get(`${rootUrl}/${id}`);
  },
  getMenus = () => {
    return window.API.get(`${rootUrl}/`);
  },
  createMenu = (data) => {
    return window.API.post(`${rootUrl}/`, data);
  },
  copyMenu = (id) => {
    return window.API.post(`${rootUrl}/${id}/copy`);
  },
  editMenu = (data) => {
    return window.API.put(`${rootUrl}/${data.id}`, data);
  },
  deleteMenu = (id) => {
    return window.API.delete(`${rootUrl}/${id}`);
  };

export default {
  getMenu,
  getMenus,
  createMenu,
  copyMenu,
  editMenu,
  deleteMenu,
}