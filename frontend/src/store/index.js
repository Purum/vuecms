import {createStore} from "vuex";
import admin from "./admin";
import common from "./common";

const store = createStore({
  modules: {
    admin,
    common,
  },
})

export default store;