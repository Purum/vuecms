import API from '../../api/common';

const common = {
  state: () => ({
    commonPage: null,
    commonLayout: null,
    commonMenus: [],
    commonChunks: [],
    commonImages: [],
  }),
  getters: {
      getCommonPage(state) {
          return state.commonPage;
      },
      getCommonLayout(state) {
          return state.commonLayout;
      },
      getCommonMenu: (state) => (name) =>{
          return state.commonMenus.find(e => e.name === name);
      },
      getCommonChunk: (state) => (name) => {
          return state.commonChunks.find(e => e.name === name);
      },
  },
  mutations: {
    setCommonPage(state, page) {
      state.commonPage = page;
    },
    setCommonLayout(state, layout) {
      state.commonLayout = layout;
    },
    addCommonMenu(state, menu) {
      state.commonMenus.push(menu);
      state.commonMenus = state.commonMenus.filter((item, index) => {
        return index !== state.commonMenus
            .lastIndexOf(e => e.id === item.id);
      })
    },
    addCommonChunk(state, chunk) {
      state.commonChunks.push(chunk);
      state.commonChunks = state.commonChunks.filter((item, index) => {
        return index !== state.commonChunks
            .lastIndexOf(e => e.id === item.id);
      })
    }
  },
  actions: {
    getCommonPage(context, data) {
      return API.getPage(data)
          .then(r => {
            context.commit('setCommonPage', r.data);
            return r;
          });
    },
    getCommonLayout(context, id) {
      return API.getLayout(id)
          .then(r => {
            context.commit('setCommonLayout', r.data);
            return r;
          });
    },
    getCommonMenu(context, name) {
      return API.getMenu(name)
          .then(r => {
            context.commit('addCommonMenu', r.data);
            return r;
          });
    },
    getCommonChunk(context, name) {
      return API.getChunk(name)
          .then(r => {
            context.commit('addCommonChunk', r.data);
            return r;
          });
    },
  },
}

export default common;