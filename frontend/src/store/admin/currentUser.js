import API from '../../api/currentUser'

const currentUser = {
  state: () => ({
    currentUser: {},
  }),
  getters: {
    getCurrentUser(state) {
      return state.currentUser;
    }
  },
  mutations: {
    setCurrentUser(state, data) {
      state.currentUser = data;
    }
  },
  actions: {
    getCurrentUser(context) {
      return API.getCurrentUser()
        .then(r => {
          context.commit('setCurrentUser', r.data);
        });
    },
    login(context, data) {
      return API.login(data);
    },
    logout() {
      return API.logout();
    },
    refresh() {
      return API.refresh();
    },
  },
}

export default currentUser;