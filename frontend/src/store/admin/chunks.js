import API from '../../api/chunks';

const chunks = {
  state: () => ({
    chunks: [],
    chunk: {},
  }),
  getters: {
    getChunks(state) {
      return state.chunks;
    },
    getChunk(state) {
      return state.chunk;
    }
  },
  mutations: {
    setChunks(state, chunks) {
      state.chunks = chunks;
    },
    setChunk(state, chunk) {
      state.chunk = chunk;
    },
    pushChunk(state, chunk) {
      state.chunks.push(chunk);
    },
    changeChunk(state, chunk) {
      state.chunks = state.chunks.map(e => e.id === chunk.id ? chunk : e);
    },
    deleteChunk(state, id) {
      state.chunks = state.chunks.filter(e => e.id !== id);
    },
  },
  actions: {
    getChunks(context) {
      return API.getChunks()
        .then(r => {
          context.commit('setChunks', r.data);
          return r;
        })
    },
    getChunk(context, id) {
      return API.getChunk(id)
        .then(r => {
          context.commit('setChunk', r.data);
          return r;
        });
    },
    createChunk(context, data) {
      return API.createChunk(data)
        .then(r => {
          context.commit('pushChunk', r.data);
          return r;
        });
    },
    copyChunk(context, id) {
      return API.copyChunk(id)
        .then(r => {
          context.commit('pushChunk', r.data);
          return r;
        });
    },
    editChunk(context, data) {
      return API.editChunk(data)
        .then(r => {
          context.commit('changeChunk', r.data);
          return r;
        });
    },
    deleteChunk(context, id) {
      return API.deleteChunk(id)
        .then(r => {
          context.commit('deleteChunk', id);
          return r;
        });
    }
  },
}

export default chunks;