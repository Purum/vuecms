import API from '../../api/files';

const files = {
  state: () => ({
    files: [],
  }),
  getters: {
    getFiles(state) {
      return state.files;
    },
  },
  mutations: {
    setFiles(state, data) {
      state.files = data;
    },
    appendFile(state, data) {
      state.files.push(data);
    },
    deleteFile(state, id) {
      state.files = state.files.filter(e => e.id !== id);
    }
  },
  actions: {
    getFiles(context) {
      return API.getFiles()
        .then(r => {
          context.commit('setFiles', r.data);
          return r;
        })
    },
    createFile(context, data) {
      return API.createFile(data)
          .then(r => {
            context.commit('appendFile', r.data);
            return r;
          });
    },
    deleteFile(context, id) {
      return API.deleteFile(id)
        .then(r => {
          context.commit('deleteFile', id);
          return r;
        });
    }
  },
}

export default files;