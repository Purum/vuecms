import API from '../../api/pages';

const pages = {
  state: () => ({
    pages: [],
    page: {},
  }),
  getters: {
    getPages(state) {
      return state.pages;
    },
    getPage(state) {
      return state.page;
    }
  },
  mutations: {
    setPages(state, pages) {
      state.pages = pages;
    },
    setPage(state, page) {
      state.page = page;
    },
    pushPage(state, page) {
      state.pages.push(page);
    },
    changePage(state, page) {
      state.pages = state.pages.map(e => e.id === page.id ? page : e);
    },
    deletePage(state, id) {
      state.pages = state.pages.filter(e => e.id !== id);
    },
  },
  actions: {
    getPages(context) {
      return API.getPages()
        .then(r => {
          context.commit('setPages', r.data);
          return r;
        })
    },
    getPage(context, id) {
      return API.getPage(id)
        .then(r => {
          context.commit('setPage', r.data);
          return r;
        });
    },
    createPage(context, data) {
      return API.createPage(data)
        .then(r => {
          context.commit('pushPage', r.data);
          return r;
        });
    },
    copyPage(context, id) {
      return API.copyPage(id)
        .then(r => {
          context.commit('pushPage', r.data);
          return r;
        });
    },
    editPage(context, data) {
      return API.editPage(data)
        .then(r => {
          context.commit('changePage', r.data);
          return r;
        });
    },
    deletePage(context, id) {
      return API.deletePage(id)
        .then(r => {
          context.commit('deletePage', id);
          return r;
        });
    }
  },
}

export default pages;