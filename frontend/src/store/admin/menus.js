import API from '../../api/menus';

const menus = {
  state: () => ({
    menus: [],
    menu: {},
  }),
  getters: {
    getMenus(state) {
      return state.menus;
    },
    getMenu(state) {
      return state.menu;
    }
  },
  mutations: {
    setMenus(state, menus) {
      state.menus = menus;
    },
    setMenu(state, menu) {
      state.menu = menu;
    },
    pushMenu(state, menu) {
      state.menus.push(menu);
    },
    changeMenu(state, menu) {
      state.menus = state.menus.map(e => e.id === menu.id ? menu : e);
    },
    deleteMenu(state, id) {
      state.menus = state.menus.filter(e => e.id !== id);
    },
  },
  actions: {
    getMenus(context) {
      return API.getMenus()
        .then(r => {
          context.commit('setMenus', r.data);
          return r;
        })
    },
    getMenu(context, id) {
      return API.getMenu(id)
        .then(r => {
          context.commit('setMenu', r.data);
          return r;
        });
    },
    createMenu(context, data) {
      return API.createMenu(data)
        .then(r => {
          context.commit('pushMenu', r.data);
          return r;
        });
    },
    copyPage(context, id) {
      return API.copyPage(id)
        .then(r => {
          context.commit('pushPage', r.data);
          return r;
        });
    },
    editMenu(context, data) {
      return API.editMenu(data)
        .then(r => {
          context.commit('changeMenu', r.data);
          return r;
        });
    },
    deleteMenu(context, id) {
      return API.deleteMenu(id)
        .then(r => {
          context.commit('deleteMenu', id);
          return r;
        });
    }
  },
}

export default menus;