import API from '../../api/layouts';

const layouts = {
  state: () => ({
    layouts: [],
    layout: {},
  }),
  getters: {
    getLayouts(state) {
      return state.layouts;
    },
    getLayout(state) {
      return state.layout;
    }
  },
  mutations: {
    setLayouts(state, layouts) {
      state.layouts = layouts;
    },
    setLayout(state, layout) {
      state.layout = layout;
    },
    pushLayout(state, layout) {
      state.layouts.push(layout);
    },
    changeLayout(state, layout) {
      state.layouts = state.layouts.map(e => e.id === layout.id ? layout : e);
    },
    deleteLayout(state, id) {
      state.layouts = state.layouts.filter(e => e.id !== id);
    },
  },
  actions: {
    getLayouts(context) {
      return API.getLayouts()
        .then(r => {
          context.commit('setLayouts', r.data);
          return r;
        })
    },
    getLayout(context, id) {
      return API.getLayout(id)
        .then(r => {
          context.commit('setLayout', r.data);
          return r;
        });
    },
    createLayout(context, data) {
      return API.createLayout(data)
          .then(r => {
            context.commit('pushLayout', r.data);
            return r;
          });
    },
    copyLayout(context, id) {
      return API.copyLayout(id)
        .then(r => {
          context.commit('pushLayout', r.data);
          return r;
        });
    },
    editLayout(context, data) {
      return API.editLayout(data)
        .then(r => {
          context.commit('changeLayout', r.data);
          return r;
        });
    },
    deleteLayout(context, id) {
      return API.deleteLayout(id)
        .then(r => {
          context.commit('deleteLayout', id);
          return r;
        });
    }
  },
}

export default layouts;