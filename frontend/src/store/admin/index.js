import layouts from "./layouts.js";
import pages from "./pages.js";
import menus from "./menus.js";
import chunks from "./chunks.js";
import files from "./files.js";
import currentUser from "./currentUser";

const admin = {
  state: () => ({
    tabs: [
      'layouts',
      'pages',
      'menus',
      'chunks',
      'files',
    ],
  }),
  getters: {
    getTabs(state) {
      return state.tabs;
    },
  },
  mutations: {

  },
  actions: {

  },
  modules: {
    layouts,
    pages,
    menus,
    chunks,
    files,
    currentUser,
  }
}

export default admin;