import { createApp } from 'vue/dist/vue.esm-bundler';
import './assets/css/styles.scss'
import App from './App.vue';
import './api/api';
import router from './router';
import store from "./store";
import Chunk from "./components/common/Chunk.vue";
import Menu from "./components/common/Menu.vue";
import Image from "./components/shared/Image.vue";

const app = createApp(App);

app.use(router);
app.use(store);

app.component('common-chunk', Chunk);
app.component('common-menu', Menu);
app.component('shared-image', Image);

app.mount('#app');
