import { createRouter, createWebHistory } from 'vue-router'
import Auth from '../views/Auth.vue';
import Page from '../views/Page.vue';
import Page404 from '../views/Page404.vue';
import ItemsList from "../views/admin/ItemsList.vue";
import LayoutForm from "../views/admin/LayoutForm.vue";
import PageForm from "../views/admin/PageForm.vue";
import MenuForm from "../views/admin/MenuForm.vue";
import ChunkForm from "../views/admin/ChunkForm.vue";

const checkAuthorized = (to, from, next) => {
  if(window.API.validToken() !== null) {
    next();
    return;
  }
  next({name: 'auth'});
}
const checkNotAuthorized = (to, from, next) => {
  if(window.API.validToken() === null) {
    next();
    return;
  }
  next({name: 'layouts'});
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  linkExactActiveClass: "active",
  routes: [
    {
      path: '/auth',
      name: 'auth',
      component: Auth,
      beforeEnter: [checkNotAuthorized],
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import('../views/admin/Admin.vue'),
      redirect: () => {
        return {name: 'layouts'};
      },
      beforeEnter: [checkAuthorized],
      children: [
        {
          path: 'layouts',
          name: 'layouts',
          component: ItemsList,
          meta: {
            name: 'Layout',
          },
          children: [
            {
              path: 'new',
              name: 'newLayout',
              component: LayoutForm,
              meta: {
                showModal: true,
              }
            },
            {
              path: 'edit/:layoutId',
              name: 'editLayout',
              component: LayoutForm,
              meta: {
                showModal: true,
              }
            }
          ],
        },
        {
          path: 'pages',
          name: 'pages',
          component: ItemsList,
          meta: {
            name: 'Page',
          },
          children: [
            {
              path: 'new',
              name: 'newPage',
              component: PageForm,
              meta: {
                showModal: true,
              }
            },
            {
              path: 'edit/:pageId',
              name: 'editPage',
              component: PageForm,
              meta: {
                showModal: true,
              }
            }
          ],
        },
        {
          path: 'menus',
          name: 'menus',
          component: ItemsList,
          meta: {
            name: 'Menu',
          },
          children: [
            {
              path: 'new',
              name: 'newMenu',
              component: MenuForm,
              meta: {
                showModal: true,
              }
            },
            {
              path: 'edit/:menuId',
              name: 'editMenu',
              component: MenuForm,
              meta: {
                showModal: true,
              }
            }
          ],
        },
        {
          path: 'chunks',
          name: 'chunks',
          component: ItemsList,
          meta: {
            name: 'Chunk',
          },
          children: [
            {
              path: 'new',
              name: 'newChunk',
              component: ChunkForm,
              meta: {
                showModal: true,
              }
            },
            {
              path: 'edit/:chunkId',
              name: 'editChunk',
              component: ChunkForm,
              meta: {
                showModal: true,
              }
            }
          ],
        },
        {
          path: 'files',
          name: 'files',
          component: () => import('../views/admin/Files.vue'),
          meta: {
            name: 'File',
          },
        },
      ],
    },
    {
      path: '/404',
      name: '404',
      component: Page404,
    },
    {
      path: '/:pageUri*',
      name: 'page',
      component: Page,
    },
  ]
})

export default router
