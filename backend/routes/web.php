<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->post('login', 'AuthController@login');
$router->post('logout', 'AuthController@logout');
$router->post('refresh', 'AuthController@refresh');
$router->get('user-profile', 'AuthController@me');

$router->group(
    ['prefix' => 'layouts'],
    function () use ($router) {
        $router->get('', 'LayoutController@getAll');
        $router->get('{id}', 'LayoutController@get');
        $router->post('', 'LayoutController@create');
        $router->post('{id}/copy', 'LayoutController@copy');
        $router->put('{id}', 'LayoutController@update');
        $router->delete('{id}', 'LayoutController@delete');
    }
);

$router->group(
    ['prefix' => 'pages'],
    function () use ($router) {
        $router->get('', 'PageController@getAll');
        $router->get('{id}', 'PageController@get');
        $router->post('', 'PageController@create');
        $router->post('{id}/copy', 'PageController@copy');
        $router->put('{id}', 'PageController@update');
        $router->delete('{id}', 'PageController@delete');
    }
);

$router->group(
    ['prefix' => 'menus'],
    function () use ($router) {
        $router->get('', 'MenuController@getAll');
        $router->get('{id}', 'MenuController@get');
        $router->post('', 'MenuController@create');
        $router->post('{id}/copy', 'MenuController@copy');
        $router->put('{id}', 'MenuController@update');
        $router->delete('{id}', 'MenuController@delete');
    }
);

$router->group(
    ['prefix' => 'chunks'],
    function () use ($router) {
        $router->get('', 'ChunkController@getAll');
        $router->get('{id}', 'ChunkController@get');
        $router->post('', 'ChunkController@create');
        $router->post('{id}/copy', 'ChunkController@copy');
        $router->put('{id}', 'ChunkController@update');
        $router->delete('{id}', 'ChunkController@delete');
    }
);

$router->group(
    ['prefix' => 'files'],
    function () use ($router) {
        $router->get('', 'FileController@getAll');
        $router->get('{name}', 'FileController@get');
        $router->post('', 'FileController@create');
        $router->delete('{name}', 'FileController@delete');
    }
);

$router->get('/common/page', 'CommonController@page');
$router->get('/common/layout/{id}', 'CommonController@layout');
$router->get('/common/menu/{name}', 'CommonController@menu');
$router->get('/common/chunk/{name}', 'CommonController@chunk');
$router->get('/common/image', 'CommonController@image');

