<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Layout;

class SlotName extends Model
{
    //
    protected $fillable = [
        'layout_id',
        'name',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
