<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chunk extends Model
{
    //
    protected $fillable = [
        'name',
        'content',
    ];
}
