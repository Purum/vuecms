<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //

    protected $fillable = [
        'name',
        'uri',
        'parent_id',
        'layout_id',
    ];
    protected $hidden = [
      'created_at',
      'updated_at',
    ];

    public function slots() {
        return $this->hasMany(Slot::class)
            ->with('slotName')
            ->select(['id', 'page_id', 'slot_name_id', 'content']);
    }

    public function parent() {
        return $this->belongsTo(Page::class);
    }

    public function setSlots($slots) {
        $this->removeSlots();
        foreach ($slots as $slot) {
            $this->slots()
                ->create($slot);
        }
    }

    public function removeSlots() {
        $ids = [];
        $slots = $this->slots()->get();
        foreach ($slots as $slot) {
            $ids[] = $slot['id'];
        }
        Slot::destroy($ids);
    }
}
