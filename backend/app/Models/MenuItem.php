<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    //

    protected $fillable = [
        'menu_id',
        'page_id',
        'name',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function page() {
        return $this->belongsTo(Page::class);
    }

    public function pageUri() {
        return $this->page()['uri'];
    }
}
