<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    //
    protected $fillable = [
        'slot_name_id',
        'page_id',
        'content',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function slotName() {
         return $this->belongsTo(SlotName::class)
             ->select('id', 'name');
    }
}
