<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //

    protected $fillable = [
        'name',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function menuItems() {
        return $this->hasMany(MenuItem::class)
            ->with('page');
    }

    public function setMenuItems($items) {
        foreach ($items as $item) {
            if (array_key_exists('id', $item)) {
                $this->menuItems()
                    ->find($item['id'])
                    ->update($item);
            } else {
                $this->menuItems()
                    ->create($item);
            }
        }
    }

    public function removeMenuItems($items) {
        $ids = [];
        foreach ($items as $item) {
            $ids[] = $item['id'];
        }
        SlotName::destroy($ids);
    }
}
