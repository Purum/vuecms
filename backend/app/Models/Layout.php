<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Layout extends Model
{
    //
    protected $fillable = [
        'name',
        'content',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function slotNames() {
        return $this->hasMany(SlotName::class)
            ->select(['id', 'name', 'layout_id']);
    }

    public function setSlots($slots) {
        foreach ($slots as $slot) {
            if (array_key_exists('id', $slot)) {
                $this->slotNames()
                    ->find($slot['id'])
                    ->update($slot);
            } else {
                $this->slotNames()
                    ->create($slot);
            }
        }
    }

    public function removeSlots($slots) {
        $ids = [];
        foreach ($slots as $slot) {
            $ids[] = $slot['id'];
        }
        SlotName::destroy($ids);
    }
}
