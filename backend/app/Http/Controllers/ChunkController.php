<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Chunk;

class ChunkController extends Controller
{
    public function __construct(Request $request)
        {

        }

        public function getAll(Request $request) {
            $params = $request->collect();
            $chunks = Chunk::all();
            return response()->json($chunks);
        }

        public function get(Request $request, $id) {
            $params = $request->collect();
            $chunk = Chunk::findOrFail($id);
            return response()->json($chunk);
        }

        public function create(Request $request) {
            $params = $request->only([
                'name',
                'content',
            ]);
            $chunk = Chunk::create($params);
            return response()->json($chunk);
        }

        public function update(Request $request, $id) {
            $params = $request->only([
                'name',
                'content',
            ]);
            $chunk = Chunk::findOrFail($id)
                        ->update($params);
            return response()->json($chunk);
        }

        public function delete(Request $request, $id) {
            $res = Chunk::destroy($id);
            return response()->json($res);
        }
}
