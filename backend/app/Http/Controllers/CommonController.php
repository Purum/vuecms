<?php

namespace App\Http\Controllers;

use App\Models\Chunk;
use App\Models\Layout;
use App\Models\Menu;
use App\Models\Page;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    //

    public function page(Request $request) {
        $uri = $request->input('uri');
        $page = Page::with('slots')
            ->where('uri', $uri)
            ->orWhere('uri', $uri.'/')
            ->first();
        return response()->json($page);
    }

    public function layout(Request $request, $id) {
        $layout = Layout::findOrFail($id);
        return response()->json($layout);
    }

    public function menu(Request $request, $name) {
        $menu = Menu::with('menuItems')->firstWhere('name', $name);
        return response()->json($menu);
    }

    public function chunk(Request $request, $name) {
        $chunk = Chunk::firstWhere('name', $name);
        return response()->json($chunk);
    }

    public function image(Request $request, $name) {


    }

}
