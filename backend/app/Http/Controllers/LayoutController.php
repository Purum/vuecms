<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Layout;

class LayoutController extends Controller
{

    public function __construct(Request $request)
    {

    }
    public function getAll(Request $request) {
        $params = $request->collect();
        $layouts = Layout::all();
        return response()->json($layouts);
    }

    public function get(Request $request, $id) {
        $params = $request->collect();
        $layout = Layout::with('slotNames')->findOrFail($id);
        return response()->json($layout);
    }

    public function create(Request $request) {
        $params = $request->only([
            'name',
            'content',
        ]);
        $layout = Layout::create($params);

        $slotNames = $request->get('slot_names');
        $layout->setSlots($slotNames);

        return response()->json($layout);
    }

    public function copy(Request $request) {
        $id = $request->get('id');
        $layout = Layout::findOrFail($id);
        $new = $layout->replicate();
        $new->name .= " copy";
        $new->push();
        $new->setSlots($layout->slotNames()->get());
        return response()->json($new);
    }

    public function update(Request $request, $id) {
        $params = $request->only([
            'name',
            'content'
        ]);
        $layout = Layout::findOrFail($id);
        $layout->update($params);

        $slotNames = $request->get('slot_names');
        $layout->setSlots($slotNames);

        $removedSlots = $request->get('removed_slots');
        $layout->removeSlots($removedSlots);

        return response()->json($layout);
    }

    public function delete(Request $request, $id) {
        $res = Layout::findOrFail($id)->delete();
        return response()->json($res);
    }
}
