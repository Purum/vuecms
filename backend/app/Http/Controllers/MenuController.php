<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{

    public function __construct(Request $request)
    {

    }

    public function getAll(Request $request) {
        $params = $request->collect();
        $menus = Menu::all();
        return response()->json($menus);
    }

    public function get(Request $request, $id) {
        $params = $request->only([
            'name',
        ]);
        $menu = Menu::with('menuItems')->findOrFail($id);
        return response()->json($menu);
    }

    public function create(Request $request) {
        $params = $request->only([
            'name',
        ]);
        $menu = Menu::create($params);

        $items = $request->get('menu_items');
        $menu->setMenuItems($items);

        return response()->json($menu);
    }

    public function update(Request $request, $id) {
        $params = $request->only([
            'name',
        ]);
        $menu = Menu::findOrFail($id);
        $menu->update($params);

        $items = $request->get('menu_items');
        $menu->setMenuItems($items);

        $removedItems = $request->get('removed_items');
        $menu->removeMenuItems($removedItems);

        return response()->json($menu);
    }

    public function delete(Request $request, $id) {
        $res = Menu::destroy($id);
        return response()->json($res);
    }
}
