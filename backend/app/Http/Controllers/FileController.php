<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class FileController extends Controller
{
    //

    public function getAll(Request $request) {
        return Storage::disk('public')->allFiles();
    }

    public function get(Request $request, $name) {
        return Storage::disk('public')->get($name);
    }

    public function create(Request $request) {
        if($request->hasFile('myFile')) {
            $file = $request->file('myFile');
            $name = time() . $file->getClientOriginalName();
            return Storage::disk('public')->put($name, $file);
        }
        return false;
    }

    public function delete(Request $request, $name) {
        return Storage::disk('public')->delete($name);
    }
}
