<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    public function __construct(Request $request)
    {

    }

    public function getAll(Request $request) {
        $params = $request->collect();
        $pages = Page::all();
        return response()->json($pages);
    }

    public function get(Request $request, $id) {
        $params = $request->collect();
        $page = Page::with('slots')->findOrFail($id);

        return response()->json($page);
    }

    public function create(Request $request) {
        $params = $request->only([
            'name',
            'uri',
            'parent_id',
            'layout_id',
        ]);
        $page = Page::create($params);

        $slots = $request->get('slots');
        $page->setSlots($slots);

        return response()->json($page);
    }

    public function copy(Request $request) {
        $id = $request->get('id');
        $layout = Page::findOrFail($id);
        $new = $layout->replicate();
        $new->name .= " copy";
        $new->push();
        $new->setSlots($layout->slots()->get());
        return response()->json($new);
    }

    public function update(Request $request, $id) {
        $params = $request->only([
            'name',
            'uri',
            'parent_id',
            'layout_id',
        ]);
        $page = Page::findOrFail($id);
        $page->update($params);

        $slots = $request->get('slots');
        $page->setSlots($slots);

        return response()->json($page);
    }

    public function delete(Request $request, $id) {
        $res = Page::findOrFail($id)->delete();
        return response()->json($res);
    }
}
