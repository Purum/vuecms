<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\Page;
use App\Models\Layout;

class CreatePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(Layout::class);
            $table->foreignId('parent_id')
                    ->nullable()
                    ->references('id')
                    ->on('pages');
            $table->string('uri')->unique();
            $table->string('name')->unique();
            $table->boolean('visible')
                  ->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
